﻿using System;
using System.IO.Ports;
using System.Threading;

namespace ControllerClient
{
    internal class Program
    {
        static SerialPort sp;

        static void InitPort(string portName, int baudRate)
        {
            sp = new SerialPort(portName, baudRate);
            sp.Open();
        }

        static void UsePort()
        {
            string msg = sp.ReadExisting();
            Console.WriteLine(msg);
            Thread.Sleep(100);
        }

        static void Main(string[] args)
        {
            InitPort("COM5", 9600);
            while (true)
            {
                UsePort();
            }
        }
    }
}
